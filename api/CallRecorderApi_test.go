package api

import (
	"fmt"
	"go-call-recorder-api/model"
	"go-call-recorder-api/utils"
	"testing"
)

/**
 * Test case for buyCreditsPost
 */
func TestBuyCredits(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.BuyCreditsRequest{
		ApiKey:     apiKey,
		Amount:     100,
		Receipt:    "test",
		ProductId:  1,
		DeviceType: "ios",
	}
	response, err := api.BuyCredits(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for cloneFilePost
 */
func TestCloneFile(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, fileId := api.provideApiKeyAndFileId()
	request := model.CloneFileRequest{
		ApiKey: apiKey,
		Id:     fileId,
	}
	response, err := api.CloneFile(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for createFilePost
 */
func TestCreateFile(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.CreateFileRequest{
		ApiKey: apiKey,
		File:   "/tmp/audio.mp3",
		Data: model.CreateFileData{
			Name:       "test-file",
			Email:      "e@mail.com",
			Phone:      "+16463742122",
			LName:      "l",
			FName:      "f",
			Notes:      "n",
			Tags:       "t",
			Source:     "0",
			RemindDays: "10",
			RemindDate: "2019-09-03T21:11:51.824121+03:00",
		},
	}
	response, err := api.CreateFile(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for createFolderPost
 */
func TestCreateFolder(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.CreateFolderRequest{
		ApiKey: apiKey,
		Name:   "test-folder",
		Pass:   "12345",
	}
	response, err := api.CreateFolder(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for deleteFilesPost
 */
func TestDeleteFiles(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, _ := api.provideApiKeyAndFileId()
	request := model.DeleteFilesRequest{
		ApiKey: apiKey,
		Ids:    []int64{1344, 1345},
		Action: "remove_forever",
	}
	response, err := api.DeleteFiles(request)
	fmt.Println(response)
	fmt.Println(err)
}

func TestA(t *testing.T) {
	fmt.Println(utils.ToString([]int64{1, 2}))
}

/**
 * Test case for deleteFolderPost
 */
func TestDeleteFolder(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, folderId := api.provideApiKeyAndFolderId()
	request := model.DeleteFolderRequest{
		ApiKey: apiKey,
		Id:     folderId,
		MoveTo: 1,
	}
	response, err := api.DeleteFolder(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for deleteMetaFilesPost
 */
func TestDeleteMetaFiles(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, fileId, metaFileId := api.provideApiKeyAndFileIdAndMetaFileId()
	request := model.DeleteMetaFilesRequest{
		ApiKey:   apiKey,
		Ids:      []int64{metaFileId},
		ParentId: fileId,
	}
	response, err := api.DeleteMetaFiles(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for getFilesPost
 */
func TestGetFiles(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.GetFilesRequest{
		ApiKey:   apiKey,
		Page:     "0",
		FolderId: 0,
		Source:   "all",
		Pass:     "12345",
		Reminder: false,
		Q:        "hello",
		Id:       0,
		Op:       "greater",
	}
	response, err := api.GetFiles(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for getFoldersPost
 */
func TestGetFolders(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.GetFoldersRequest{
		ApiKey: apiKey,
	}
	response, err := api.GetFolders(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for getLanguagesPost
 */
func TestGetLanguages(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.GetLanguagesRequest{
		ApiKey: apiKey,
	}
	response, err := api.GetLanguages(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for getMetaFilesPost
 */
func TestGetMetaFiles(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, fileId := api.provideApiKeyAndFileId()
	request := model.GetMetaFilesRequest{
		ApiKey:   apiKey,
		ParentId: fileId,
	}
	response, err := api.GetMetaFiles(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for getMsgsPost
 */
func TestGetMsgs(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.GetMessagesRequest{
		ApiKey: apiKey,
	}
	response, err := api.GetMsgs(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for getPhonesPost
 */
func TestGetPhones(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.GetPhonesRequest{
		ApiKey: apiKey,
	}
	response, err := api.GetPhones(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for getProfilePost
 */
func TestGetProfile(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.GetProfileRequest{
		ApiKey: apiKey,
	}
	response, err := api.GetProfile(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for getSettingsPost
 */
func TestGetSettings(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.GetSettingsRequest{
		ApiKey: apiKey,
	}
	response, err := api.GetSettings(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for getTranslationsPost
 */
func TestGetTranslations(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.GetTranslationsRequest{
		ApiKey:   apiKey,
		Language: "en_US",
	}
	response, err := api.GetTranslations(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for notifyUserCustomPost
 */
func TestNotifyUserCustom(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.NotifyUserRequest{
		ApiKey:     apiKey,
		Title:      "test-title",
		Body:       "test-body",
		DeviceType: "ios",
	}
	response, err := api.NotifyUserCustom(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for recoverFilePost
 */
func TestRecoverFile(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, fileId := api.provideApiKeyAndFileId()
	request := model.RecoverFileRequest{
		ApiKey:   apiKey,
		Id:       fileId,
		FolderId: 0,
	}
	response, err := api.RecoverFile(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for registerPhonePost
 */
func TestRegisterPhone(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	request := model.RegisterPhoneRequest{
		Token: "55942ee3894f51000530894",
		Phone: "+16463742122",
	}
	response, err := api.RegisterPhone(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for updateDeviceTokenPost
 */
func TestUpdateDeviceToken(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.UpdateDeviceTokenRequest{
		ApiKey:      apiKey,
		DeviceToken: "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceType:  "ios",
	}
	response, err := api.UpdateDeviceToken(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for updateFilePost
 */
func TestUpdateFile(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, fileId := api.provideApiKeyAndFileId()
	request := model.UpdateFileRequest{
		ApiKey:     apiKey,
		Id:         fileId,
		FName:      "f",
		LName:      "l",
		Notes:      "n",
		Email:      "e@mail.ru",
		Phone:      "+16463742122",
		Tags:       "t",
		FolderId:   0,
		Name:       "n",
		RemindDays: "10",
		RemindDate: "2019-09-03T21:11:51.824121+03:00",
	}
	response, err := api.UpdateFile(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for updateFolderPost
 */
func TestUpdateFolder(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, folderId := api.provideApiKeyAndFolderId()
	request := model.UpdateFolderRequest{
		ApiKey:    apiKey,
		Id:        folderId,
		Name:      "n",
		Pass:      "12345",
		IsPrivate: true,
	}
	response, err := api.UpdateFolder(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for updateOrderPost
 */
func TestUpdateOrder(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.UpdateOrderRequest{
		ApiKey:  apiKey,
		Folders: []model.UpdateOrderRequestFolder{},
	}
	response, err := api.UpdateOrder(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for updateProfileImgPost
 */
func TestUpdateProfileImg(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.UpdateProfileImgRequest{
		ApiKey: apiKey,
		File:   "/tmp/java.png",
	}
	response, err := api.UpdateProfileImg(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for updateProfilePost
 */
func TestUpdateProfile(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.UpdateProfileRequest{
		ApiKey: apiKey,
		Data: model.UpdateProfileRequestData{
			FName:    "f",
			LName:    "l",
			Email:    "e@mail.ru",
			IsPublic: true,
			Language: "",
		},
	}
	response, err := api.UpdateProfile(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for updateSettingsPost
 */
func TestUpdateSettings(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.UpdateSettingsRequest{
		ApiKey:          apiKey,
		PlayBeep:        "no",
		FilesPermission: "private",
	}
	response, err := api.UpdateSettings(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for updateStarPost
 */
func TestUpdateStar(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, fileId := api.provideApiKeyAndFileId()
	request := model.UpdateStarRequest{
		ApiKey: apiKey,
		Id:     fileId,
		Star:   1,
		Type:   "file",
	}
	response, err := api.UpdateStar(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for updateUserPost
 */
func TestUpdateUser(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey := api.provideApiKey()
	request := model.UpdateUserRequest{
		ApiKey:   apiKey,
		App:      "rec",
		TimeZone: "10",
	}
	response, err := api.UpdateUser(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for uploadMetaFilePost
 */
func TestUploadMetaFile(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, fileId := api.provideApiKeyAndFileId()
	request := model.UploadMetaFileRequest{
		ApiKey:   apiKey,
		File:     "/tmp/java.png",
		Name:     "test-meta",
		ParentId: fileId,
		Id:       1,
	}
	response, err := api.UploadMetaFile(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for verifyFolderPassPost
 */
func TestVerifyFolderPass(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	apiKey, folderId := api.provideApiKeyAndFolderId()
	request := model.VerifyFolderPassRequest{
		ApiKey: apiKey,
		Id:     folderId,
		Pass:   "12345",
	}
	response, err := api.VerifyFolderPass(request)
	fmt.Println(response)
	fmt.Println(err)
}

/**
 * Test case for verifyPhonePost
 */
func TestVerifyPhone(t *testing.T) {
	config := Configuration{
		Host: "https://app2.virtualbrix.net",
	}
	api := CallRecorderApi{
		Config: config,
	}
	code := api.provideCode()
	request := model.VerifyPhoneRequest{
		Token:       "55942ee3894f51000530894",
		Phone:       "+16463742122",
		Code:        code,
		Mcc:         "300",
		App:         "rec",
		DeviceToken: "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceId:    "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceType:  "ios",
		TimeZone:    "10",
	}
	response, err := api.VerifyPhone(request)
	fmt.Println(response)
	fmt.Println(err)
}

func (api *CallRecorderApi) provideCode() string {
	registerPhonePostResponse, _ := api.RegisterPhone(model.RegisterPhoneRequest{
		Token: "55942ee3894f51000530894",
		Phone: "+16463742122",
	})
	registerPhonePostResponseCode := registerPhonePostResponse.Code
	code := registerPhonePostResponseCode
	return code
}

func (api *CallRecorderApi) provideApiKey() string {
	registerPhonePostResponse, _ := api.RegisterPhone(model.RegisterPhoneRequest{
		Token: "55942ee3894f51000530894",
		Phone: "+16463742122",
	})
	registerPhonePostResponseCode := registerPhonePostResponse.Code
	verifyPhonePostResponse, _ := api.VerifyPhone(model.VerifyPhoneRequest{
		Token:       "55942ee3894f51000530894",
		Phone:       "+16463742122",
		Code:        registerPhonePostResponseCode,
		Mcc:         "300",
		App:         "rec",
		DeviceToken: "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceId:    "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceType:  "ios",
		TimeZone:    "10",
	})
	verifyPhonePostResponseApiKey := verifyPhonePostResponse.ApiKey
	apiKey := verifyPhonePostResponseApiKey
	return apiKey
}

func (api *CallRecorderApi) provideApiKeyAndFileId() (string, int64) {
	registerPhonePostResponse, _ := api.RegisterPhone(model.RegisterPhoneRequest{
		Token: "55942ee3894f51000530894",
		Phone: "+16463742122",
	})
	registerPhonePostResponseCode := registerPhonePostResponse.Code
	verifyPhonePostResponse, _ := api.VerifyPhone(model.VerifyPhoneRequest{
		Token:       "55942ee3894f51000530894",
		Phone:       "+16463742122",
		Code:        registerPhonePostResponseCode,
		Mcc:         "300",
		App:         "rec",
		DeviceToken: "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceId:    "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceType:  "ios",
		TimeZone:    "10",
	})
	verifyPhonePostResponseApiKey := verifyPhonePostResponse.ApiKey
	createFilePostResponse, _ := api.CreateFile(model.CreateFileRequest{
		ApiKey: verifyPhonePostResponseApiKey,
		File:   "/tmp/audio.mp3",
		Data: model.CreateFileData{
			Name:       "test-file",
			Email:      "e@mail.com",
			Phone:      "+16463742122",
			LName:      "l",
			FName:      "f",
			Notes:      "n",
			Tags:       "t",
			Source:     "0",
			RemindDays: "10",
			RemindDate: "2019-09-03T21:11:51.824121+03:00",
		},
	})
	createFilePostResponseId := createFilePostResponse.Id
	apiKey := verifyPhonePostResponseApiKey
	fileId := createFilePostResponseId
	return apiKey, fileId
}

func (api *CallRecorderApi) provideApiKeyAndFolderId() (string, int64) {
	registerPhonePostResponse, _ := api.RegisterPhone(model.RegisterPhoneRequest{
		Token: "55942ee3894f51000530894",
		Phone: "+16463742122",
	})
	registerPhonePostResponseCode := registerPhonePostResponse.Code
	verifyPhonePostResponse, _ := api.VerifyPhone(model.VerifyPhoneRequest{
		Token:       "55942ee3894f51000530894",
		Phone:       "+16463742122",
		Code:        registerPhonePostResponseCode,
		Mcc:         "300",
		App:         "rec",
		DeviceToken: "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceId:    "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceType:  "ios",
		TimeZone:    "10",
	})
	verifyPhonePostResponseApiKey := verifyPhonePostResponse.ApiKey
	createFolderPostResponse, _ := api.CreateFolder(model.CreateFolderRequest{
		ApiKey: verifyPhonePostResponseApiKey,
		Name:   "test-folder",
		Pass:   "12345",
	})
	createFolderPostResponseId := createFolderPostResponse.Id
	apiKey := verifyPhonePostResponseApiKey
	folderId := createFolderPostResponseId
	return apiKey, folderId
}

func (api *CallRecorderApi) provideApiKeyAndFileIdAndMetaFileId() (string, int64, int64) {
	registerPhonePostResponse, _ := api.RegisterPhone(model.RegisterPhoneRequest{
		Token: "55942ee3894f51000530894",
		Phone: "+16463742122",
	})
	registerPhonePostResponseCode := registerPhonePostResponse.Code
	verifyPhonePostResponse, _ := api.VerifyPhone(model.VerifyPhoneRequest{
		Token:       "55942ee3894f51000530894",
		Phone:       "+16463742122",
		Code:        registerPhonePostResponseCode,
		Mcc:         "300",
		App:         "rec",
		DeviceToken: "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceId:    "871284c348e04a9cacab8aca6b2f3c9a",
		DeviceType:  "ios",
		TimeZone:    "10",
	})
	verifyPhonePostResponseApiKey := verifyPhonePostResponse.ApiKey
	createFilePostResponse, _ := api.CreateFile(model.CreateFileRequest{
		ApiKey: verifyPhonePostResponseApiKey,
		File:   "/tmp/audio.mp3",
		Data: model.CreateFileData{
			Name:       "test-file",
			Email:      "e@mail.com",
			Phone:      "+16463742122",
			LName:      "l",
			FName:      "f",
			Notes:      "n",
			Tags:       "t",
			Source:     "0",
			RemindDays: "10",
			RemindDate: "2019-09-03T21:11:51.824121+03:00",
		},
	})
	createFilePostResponseId := createFilePostResponse.Id
	uploadMetaFilePostResponse, _ := api.UploadMetaFile(model.UploadMetaFileRequest{
		ApiKey:   verifyPhonePostResponseApiKey,
		File:     "/tmp/java.png",
		Name:     "test-meta",
		ParentId: createFilePostResponseId,
		Id:       1,
	})
	uploadMetaFilePostResponseId := uploadMetaFilePostResponse.Id
	apiKey := verifyPhonePostResponseApiKey
	fileId := createFilePostResponseId
	metaFileId := uploadMetaFilePostResponseId
	return apiKey, fileId, metaFileId
}
