package api

import (
	"encoding/json"
	"go-call-recorder-api/model"
	"go-call-recorder-api/utils"
	"io/ioutil"
	"net/http"
	"net/url"
)

type CallRecorderApi struct {
	Config Configuration
}

func (api *CallRecorderApi) BuyCredits(request model.BuyCreditsRequest) (model.BuyCreditsResponse, error) {
	data := url.Values{
		"api_key":     {(request.ApiKey)},
		"amount":      {utils.ToString(request.Amount)},
		"receipt":     {(request.Receipt)},
		"product_id":  {utils.ToString(request.ProductId)},
		"device_type": {(request.DeviceType)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/buy_credits", data)
	if err != nil {
		return model.BuyCreditsResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.BuyCreditsResponse{}, err
	}
	var response model.BuyCreditsResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) CloneFile(request model.CloneFileRequest) (model.CloneFileResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
		"id":      {utils.ToString(request.Id)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/clone_file", data)
	if err != nil {
		return model.CloneFileResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.CloneFileResponse{}, err
	}
	var response model.CloneFileResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) CreateFile(request model.CreateFileRequest) (model.CreateFileResponse, error) {
	data := map[string]string{
		"api_key": (request.ApiKey),
		"data":    utils.ToString(request.Data),
	}
	req, err := utils.NewFileUploadRequest(api.Config.Host+"/rapi/create_file", data, "file", request.File)
	if err != nil {
		return model.CreateFileResponse{}, err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return model.CreateFileResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.CreateFileResponse{}, err
	}
	var response model.CreateFileResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) CreateFolder(request model.CreateFolderRequest) (model.CreateFolderResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
		"name":    {(request.Name)},
		"pass":    {(request.Pass)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/create_folder", data)
	if err != nil {
		return model.CreateFolderResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.CreateFolderResponse{}, err
	}
	var response model.CreateFolderResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) DeleteFiles(request model.DeleteFilesRequest) (model.DeleteFilesResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
		"ids":     {utils.ToString(request.Ids)},
		"action":  {(request.Action)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/delete_files", data)
	if err != nil {
		return model.DeleteFilesResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.DeleteFilesResponse{}, err
	}
	var response model.DeleteFilesResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) DeleteFolder(request model.DeleteFolderRequest) (model.DeleteFolderResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
		"id":      {utils.ToString(request.Id)},
		"move_to": {utils.ToString(request.MoveTo)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/delete_folder", data)
	if err != nil {
		return model.DeleteFolderResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.DeleteFolderResponse{}, err
	}
	var response model.DeleteFolderResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) DeleteMetaFiles(request model.DeleteMetaFilesRequest) (model.DeleteMetaFilesResponse, error) {
	data := url.Values{
		"api_key":   {(request.ApiKey)},
		"ids":       {utils.ToString(request.Ids)},
		"parent_id": {utils.ToString(request.ParentId)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/delete_meta_files", data)
	if err != nil {
		return model.DeleteMetaFilesResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.DeleteMetaFilesResponse{}, err
	}
	var response model.DeleteMetaFilesResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) GetFiles(request model.GetFilesRequest) (model.GetFilesResponse, error) {
	data := url.Values{
		"api_key":   {(request.ApiKey)},
		"page":      {(request.Page)},
		"folder_id": {utils.ToString(request.FolderId)},
		"source":    {(request.Source)},
		"pass":      {(request.Pass)},
		"reminder":  {utils.ToString(request.Reminder)},
		"q":         {(request.Q)},
		"id":        {utils.ToString(request.Id)},
		"op":        {(request.Op)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/get_files", data)
	if err != nil {
		return model.GetFilesResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.GetFilesResponse{}, err
	}
	var response model.GetFilesResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) GetFolders(request model.GetFoldersRequest) (model.GetFoldersResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/get_folders", data)
	if err != nil {
		return model.GetFoldersResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.GetFoldersResponse{}, err
	}
	var response model.GetFoldersResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) GetLanguages(request model.GetLanguagesRequest) (model.GetLanguagesResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/get_languages", data)
	if err != nil {
		return model.GetLanguagesResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.GetLanguagesResponse{}, err
	}
	var response model.GetLanguagesResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) GetMetaFiles(request model.GetMetaFilesRequest) (model.GetMetaFilesResponse, error) {
	data := url.Values{
		"api_key":   {(request.ApiKey)},
		"parent_id": {utils.ToString(request.ParentId)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/get_meta_files", data)
	if err != nil {
		return model.GetMetaFilesResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.GetMetaFilesResponse{}, err
	}
	var response model.GetMetaFilesResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) GetMsgs(request model.GetMessagesRequest) (model.GetMessagesResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/get_msgs", data)
	if err != nil {
		return model.GetMessagesResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.GetMessagesResponse{}, err
	}
	var response model.GetMessagesResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) GetPhones(request model.GetPhonesRequest) (model.GetPhonesResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/get_phones", data)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var response model.GetPhonesResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) GetProfile(request model.GetProfileRequest) (model.GetProfileResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/get_profile", data)
	if err != nil {
		return model.GetProfileResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.GetProfileResponse{}, err
	}
	var response model.GetProfileResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) GetSettings(request model.GetSettingsRequest) (model.GetSettingsResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/get_settings", data)
	if err != nil {
		return model.GetSettingsResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.GetSettingsResponse{}, err
	}
	var response model.GetSettingsResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) GetTranslations(request model.GetTranslationsRequest) (model.GetTranslationsResponse, error) {
	data := url.Values{
		"api_key":  {(request.ApiKey)},
		"language": {(request.Language)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/get_translations", data)
	if err != nil {
		return model.GetTranslationsResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.GetTranslationsResponse{}, err
	}
	var response model.GetTranslationsResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) NotifyUserCustom(request model.NotifyUserRequest) (model.NotifyUserResponse, error) {
	data := url.Values{
		"api_key":     {(request.ApiKey)},
		"title":       {(request.Title)},
		"body":        {(request.Body)},
		"device_type": {(request.DeviceType)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/notify_user_custom", data)
	if err != nil {
		return model.NotifyUserResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.NotifyUserResponse{}, err
	}
	var response model.NotifyUserResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) RecoverFile(request model.RecoverFileRequest) (model.RecoverFileResponse, error) {
	data := url.Values{
		"api_key":   {(request.ApiKey)},
		"id":        {utils.ToString(request.Id)},
		"folder_id": {utils.ToString(request.FolderId)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/recover_file", data)
	if err != nil {
		return model.RecoverFileResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.RecoverFileResponse{}, err
	}
	var response model.RecoverFileResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) RegisterPhone(request model.RegisterPhoneRequest) (model.RegisterPhoneResponse, error) {
	data := url.Values{
		"token": {(request.Token)},
		"phone": {(request.Phone)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/register_phone", data)
	if err != nil {
		return model.RegisterPhoneResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.RegisterPhoneResponse{}, err
	}
	var response model.RegisterPhoneResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UpdateDeviceToken(request model.UpdateDeviceTokenRequest) (model.UpdateDeviceTokenResponse, error) {
	data := url.Values{
		"api_key":      {(request.ApiKey)},
		"device_token": {(request.DeviceToken)},
		"device_type":  {(request.DeviceType)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/update_device_token", data)
	if err != nil {
		return model.UpdateDeviceTokenResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UpdateDeviceTokenResponse{}, err
	}
	var response model.UpdateDeviceTokenResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UpdateFile(request model.UpdateFileRequest) (model.UpdateFileResponse, error) {
	data := url.Values{
		"api_key":     {(request.ApiKey)},
		"id":          {utils.ToString(request.Id)},
		"f_name":      {(request.FName)},
		"l_name":      {(request.LName)},
		"notes":       {(request.Notes)},
		"email":       {(request.Email)},
		"phone":       {(request.Phone)},
		"tags":        {(request.Tags)},
		"folder_id":   {utils.ToString(request.FolderId)},
		"name":        {(request.Name)},
		"remind_days": {(request.RemindDays)},
		"remind_date": {(request.RemindDate)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/update_file", data)
	if err != nil {
		return model.UpdateFileResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UpdateFileResponse{}, err
	}
	var response model.UpdateFileResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UpdateFolder(request model.UpdateFolderRequest) (model.UpdateFolderResponse, error) {
	data := url.Values{
		"api_key":    {(request.ApiKey)},
		"id":         {utils.ToString(request.Id)},
		"name":       {(request.Name)},
		"pass":       {(request.Pass)},
		"is_private": {utils.ToString(request.IsPrivate)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/update_folder", data)
	if err != nil {
		return model.UpdateFolderResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UpdateFolderResponse{}, err
	}
	var response model.UpdateFolderResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UpdateOrder(request model.UpdateOrderRequest) (model.UpdateOrderResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
		"folders": {utils.ToString(request.Folders)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/update_order", data)
	if err != nil {
		return model.UpdateOrderResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UpdateOrderResponse{}, err
	}
	var response model.UpdateOrderResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UpdateProfileImg(request model.UpdateProfileImgRequest) (model.UpdateProfileImgResponse, error) {
	data := map[string]string{
		"api_key": (request.ApiKey),
	}
	req, err := utils.NewFileUploadRequest(api.Config.Host+"/upload/update_profile_img", data, "file", request.File)
	if err != nil {
		return model.UpdateProfileImgResponse{}, err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return model.UpdateProfileImgResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UpdateProfileImgResponse{}, err
	}
	var response model.UpdateProfileImgResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UpdateProfile(request model.UpdateProfileRequest) (model.UpdateProfileResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
		"data":    {utils.ToString(request.Data)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/update_profile", data)
	if err != nil {
		return model.UpdateProfileResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UpdateProfileResponse{}, err
	}
	var response model.UpdateProfileResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UpdateSettings(request model.UpdateSettingsRequest) (model.UpdateSettingsResponse, error) {
	data := url.Values{
		"api_key":          {(request.ApiKey)},
		"play_beep":        {(request.PlayBeep)},
		"files_permission": {(request.FilesPermission)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/update_settings", data)
	if err != nil {
		return model.UpdateSettingsResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UpdateSettingsResponse{}, err
	}
	var response model.UpdateSettingsResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UpdateStar(request model.UpdateStarRequest) (model.UpdateStarResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
		"id":      {utils.ToString(request.Id)},
		"star":    {utils.ToString(request.Star)},
		"type":    {(request.Type)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/update_star", data)
	if err != nil {
		return model.UpdateStarResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UpdateStarResponse{}, err
	}
	var response model.UpdateStarResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UpdateUser(request model.UpdateUserRequest) (model.UpdateUserResponse, error) {
	data := url.Values{
		"api_key":   {(request.ApiKey)},
		"app":       {(request.App)},
		"time_zone": {(request.TimeZone)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/update_user", data)
	if err != nil {
		return model.UpdateUserResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UpdateUserResponse{}, err
	}
	var response model.UpdateUserResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) UploadMetaFile(request model.UploadMetaFileRequest) (model.UploadMetaFileResponse, error) {
	data := map[string]string{
		"api_key":   (request.ApiKey),
		"name":      (request.Name),
		"parent_id": utils.ToString(request.ParentId),
		"id":        utils.ToString(request.Id),
	}
	req, err := utils.NewFileUploadRequest(api.Config.Host+"/rapi/upload_meta_file", data, "file", request.File)
	if err != nil {
		return model.UploadMetaFileResponse{}, err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return model.UploadMetaFileResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.UploadMetaFileResponse{}, err
	}
	var response model.UploadMetaFileResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) VerifyFolderPass(request model.VerifyFolderPassRequest) (model.VerifyFolderPassResponse, error) {
	data := url.Values{
		"api_key": {(request.ApiKey)},
		"id":      {utils.ToString(request.Id)},
		"pass":    {(request.Pass)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/verify_folder_pass", data)
	if err != nil {
		return model.VerifyFolderPassResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.VerifyFolderPassResponse{}, err
	}
	var response model.VerifyFolderPassResponse
	json.Unmarshal(body, &response)
	return response, nil
}

func (api *CallRecorderApi) VerifyPhone(request model.VerifyPhoneRequest) (model.VerifyPhoneResponse, error) {
	data := url.Values{
		"token":        {(request.Token)},
		"phone":        {(request.Phone)},
		"code":         {(request.Code)},
		"mcc":          {(request.Mcc)},
		"app":          {(request.App)},
		"device_token": {(request.DeviceToken)},
		"device_id":    {(request.DeviceId)},
		"device_type":  {(request.DeviceType)},
		"time_zone":    {(request.TimeZone)},
	}
	resp, err := http.PostForm(api.Config.Host+"/rapi/verify_phone", data)
	if err != nil {
		return model.VerifyPhoneResponse{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.VerifyPhoneResponse{}, err
	}
	var response model.VerifyPhoneResponse
	json.Unmarshal(body, &response)
	return response, nil
}
