package utils

import (
    "encoding/json"
)

func ToString(v interface{}) string {
    bytes, _ := json.Marshal(v)
    return string(bytes)
}
