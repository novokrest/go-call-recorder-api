package model

type CreateFolderRequest struct {
    ApiKey string `json:"api_key"`
    Name string `json:"name"`
    Pass string `json:"pass"`
}

