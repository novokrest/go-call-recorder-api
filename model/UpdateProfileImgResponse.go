package model

type UpdateProfileImgResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
    Code string `json:"code"`
    File string `json:"file"`
    Path string `json:"path"`
}

