package model

type CreateFileRequest struct {
    ApiKey string `json:"api_key"`
    File string `json:"file"`
    Data CreateFileData `json:"data"`
}

