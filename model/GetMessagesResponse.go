package model

type GetMessagesResponse struct {
    Status string `json:"status"`
    Msgs []GetMessagesResponseMsg `json:"msgs"`
}

