package model

type GetSettingsResponseSettings struct {
    PlayBeep PlayBeep `json:"play_beep"`
    FilesPermission FilesPermission `json:"files_permission"`
}

