package model

type UpdateStarResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
    Code string `json:"code"`
}

