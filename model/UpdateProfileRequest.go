package model

type UpdateProfileRequest struct {
    ApiKey string `json:"api_key"`
    Data UpdateProfileRequestData `json:"data"`
}

