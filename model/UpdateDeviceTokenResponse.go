package model

type UpdateDeviceTokenResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
}

