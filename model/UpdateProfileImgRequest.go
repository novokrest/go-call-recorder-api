package model

type UpdateProfileImgRequest struct {
    ApiKey string `json:"api_key"`
    File string `json:"file"`
}

