package model

type DeleteFolderResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
}

