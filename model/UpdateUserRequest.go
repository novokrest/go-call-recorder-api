package model

type UpdateUserRequest struct {
    ApiKey string `json:"api_key"`
    App App `json:"app"`
    TimeZone string `json:"time_zone"`
}

