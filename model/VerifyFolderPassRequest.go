package model

type VerifyFolderPassRequest struct {
    ApiKey string `json:"api_key"`
    Id int64 `json:"id"`
    Pass string `json:"pass"`
}

