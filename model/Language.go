package model

type Language struct {
    Code string `json:"code"`
    Name string `json:"name"`
    Flag string `json:"flag"`
}

