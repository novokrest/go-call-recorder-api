package model

type UpdateSettingsRequest struct {
    ApiKey string `json:"api_key"`
    PlayBeep PlayBeep `json:"play_beep"`
    FilesPermission FilesPermission `json:"files_permission"`
}

