package model

type DeleteMetaFilesResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
}

