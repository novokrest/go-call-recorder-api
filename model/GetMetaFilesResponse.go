package model

type GetMetaFilesResponse struct {
    Status string `json:"status"`
    MetaFiles []GetMetaFilesResponseMetaFiles `json:"meta_files"`
}

