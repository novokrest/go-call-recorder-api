package model

type UploadMetaFileResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
    ParentId int64 `json:"parent_id"`
    Id int64 `json:"id"`
}

