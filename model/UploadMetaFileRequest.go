package model

type UploadMetaFileRequest struct {
    ApiKey string `json:"api_key"`
    File string `json:"file"`
    Name string `json:"name"`
    ParentId int64 `json:"parent_id"`
    Id int64 `json:"id"`
}

