package model

type CloneFileResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
    Code string `json:"code"`
    Id int64 `json:"id"`
}

