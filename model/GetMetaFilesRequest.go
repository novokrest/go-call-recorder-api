package model

type GetMetaFilesRequest struct {
    ApiKey string `json:"api_key"`
    ParentId int64 `json:"parent_id"`
}

