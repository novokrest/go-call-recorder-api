package model

type DeleteFilesRequest struct {
    ApiKey string `json:"api_key"`
    Ids []int64 `json:"ids"`
    Action string `json:"action"`
}

