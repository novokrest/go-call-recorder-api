package model

type GetProfileResponse struct {
    Status string `json:"status"`
    Code string `json:"code"`
    Profile GetProfileResponseProfile `json:"profile"`
    App App `json:"app"`
    ShareUrl string `json:"share_url"`
    RateUrl string `json:"rate_url"`
    Credits int64 `json:"credits"`
    CreditsTrans int64 `json:"credits_trans"`
}

