package model

type DeleteFilesResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
}

