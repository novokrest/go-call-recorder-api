package model

type CloneFileRequest struct {
    ApiKey string `json:"api_key"`
    Id int64 `json:"id"`
}

