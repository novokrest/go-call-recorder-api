package model

type GetMetaFilesResponseMetaFiles struct {
    Id int64 `json:"id"`
    ParentId int64 `json:"parent_id"`
    Name string `json:"name"`
    File string `json:"file"`
    UserId int64 `json:"user_id"`
    Time string `json:"time"`
}

