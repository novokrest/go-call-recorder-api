package model

type GetPhonesResponsePhone struct {
	PhoneNumber  string `json:"phone_number"`
	Number       string `json:"number"`
	Prefix       string `json:"prefix"`
	FriendlyName string `json:"friendly_name"`
	Flag         string `json:"flag"`
	City         string `json:"city"`
	Country      string `json:"country"`
}
