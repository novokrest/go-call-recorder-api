package model

type UpdateDeviceTokenRequest struct {
    ApiKey string `json:"api_key"`
    DeviceToken string `json:"device_token"`
    DeviceType DeviceType `json:"device_type"`
}

