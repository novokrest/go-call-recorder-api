package model

type UpdateStarRequest struct {
    ApiKey string `json:"api_key"`
    Id int64 `json:"id"`
    Star int32 `json:"star"`
    Type string `json:"type"`
}

