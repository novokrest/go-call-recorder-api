package model

type GetTranslationsResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
    Code string `json:"code"`
    Translation map[string]string `json:"translation"`
}

