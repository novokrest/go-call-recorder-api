package model

type VerifyPhoneResponse struct {
    Status string `json:"status"`
    Phone string `json:"phone"`
    ApiKey string `json:"api_key"`
    Msg string `json:"msg"`
}

