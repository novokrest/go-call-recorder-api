package model

type BuyCreditsResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
}

