package model

type GetLanguagesResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
    Languages []Language `json:"languages"`
}

