package model

type CreateFileResponse struct {
    Status string `json:"status"`
    Id int64 `json:"id"`
    Msg string `json:"msg"`
}

