package model

type DeleteMetaFilesRequest struct {
    ApiKey string `json:"api_key"`
    Ids []int64 `json:"ids"`
    ParentId int64 `json:"parent_id"`
}

