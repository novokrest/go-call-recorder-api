package model

type UpdateFileResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
}

