package model

type NotifyUserRequest struct {
    ApiKey string `json:"api_key"`
    Title string `json:"title"`
    Body string `json:"body"`
    DeviceType DeviceType `json:"device_type"`
}

