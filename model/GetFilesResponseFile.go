package model

type GetFilesResponseFile struct {
    Id int64 `json:"id"`
    AccessNumber string `json:"access_number"`
    Name string `json:"name"`
    FName string `json:"f_name"`
    LName string `json:"l_name"`
    Email string `json:"email"`
    Phone string `json:"phone"`
    Notes string `json:"notes"`
    Meta string `json:"meta"`
    Source string `json:"source"`
    Url string `json:"url"`
    Credits string `json:"credits"`
    Duration string `json:"duration"`
    Time string `json:"time"`
    ShareUrl string `json:"share_url"`
    DownloadUrl string `json:"download_url"`
}

