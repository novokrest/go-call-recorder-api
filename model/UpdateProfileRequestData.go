package model

type UpdateProfileRequestData struct {
    FName string `json:"f_name"`
    LName string `json:"l_name"`
    Email string `json:"email"`
    IsPublic bool `json:"is_public"`
    Language string `json:"language"`
}

