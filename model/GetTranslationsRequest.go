package model

type GetTranslationsRequest struct {
    ApiKey string `json:"api_key"`
    Language string `json:"language"`
}

