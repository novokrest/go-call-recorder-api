package model

type DeleteFolderRequest struct {
    ApiKey string `json:"api_key"`
    Id int64 `json:"id"`
    MoveTo int64 `json:"move_to"`
}

