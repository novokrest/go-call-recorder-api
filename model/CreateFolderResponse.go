package model

type CreateFolderResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
    Id int64 `json:"id"`
    Code string `json:"code"`
}

