package model

type GetMessagesResponseMsg struct {
    Id int64 `json:"id"`
    Title string `json:"title"`
    Body string `json:"body"`
    Time string `json:"time"`
}

