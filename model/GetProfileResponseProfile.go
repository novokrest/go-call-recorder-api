package model

type GetProfileResponseProfile struct {
    FName string `json:"f_name"`
    LName string `json:"l_name"`
    Email string `json:"email"`
    Phone string `json:"phone"`
    Pic string `json:"pic"`
    Language string `json:"language"`
    IsPublic int32 `json:"is_public"`
    PlayBeep int32 `json:"play_beep"`
    MaxLength int64 `json:"max_length"`
    TimeZone string `json:"time_zone"`
    Time int64 `json:"time"`
    Pin string `json:"pin"`
}

