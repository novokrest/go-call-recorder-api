package model

type UpdateFileRequest struct {
    ApiKey string `json:"api_key"`
    Id int64 `json:"id"`
    FName string `json:"f_name"`
    LName string `json:"l_name"`
    Notes string `json:"notes"`
    Email string `json:"email"`
    Phone string `json:"phone"`
    Tags string `json:"tags"`
    FolderId int64 `json:"folder_id"`
    Name string `json:"name"`
    RemindDays string `json:"remind_days"`
    RemindDate string `json:"remind_date"`
}

