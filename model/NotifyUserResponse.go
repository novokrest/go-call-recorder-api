package model

type NotifyUserResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
}

