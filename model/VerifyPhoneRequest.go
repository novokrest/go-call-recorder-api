package model

type VerifyPhoneRequest struct {
    Token string `json:"token"`
    Phone string `json:"phone"`
    Code string `json:"code"`
    Mcc string `json:"mcc"`
    App App `json:"app"`
    DeviceToken string `json:"device_token"`
    DeviceId string `json:"device_id"`
    DeviceType DeviceType `json:"device_type"`
    TimeZone string `json:"time_zone"`
}

