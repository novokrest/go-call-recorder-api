package model

type GetSettingsResponse struct {
    Status string `json:"status"`
    App App `json:"app"`
    Credits int64 `json:"credits"`
    Settings GetSettingsResponseSettings `json:"settings"`
}

