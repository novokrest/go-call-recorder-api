package model

type RecoverFileRequest struct {
    ApiKey string `json:"api_key"`
    Id int64 `json:"id"`
    FolderId int64 `json:"folder_id"`
}

