package model

type GetFoldersResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
    Folders []GetFoldersResponseFolder `json:"folders"`
}

