package model

type BuyCreditsRequest struct {
    ApiKey string `json:"api_key"`
    Amount int64 `json:"amount"`
    Receipt string `json:"receipt"`
    ProductId int64 `json:"product_id"`
    DeviceType DeviceType `json:"device_type"`
}

