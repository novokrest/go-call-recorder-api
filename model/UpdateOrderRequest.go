package model

type UpdateOrderRequest struct {
    ApiKey string `json:"api_key"`
    Folders []UpdateOrderRequestFolder `json:"folders"`
}

