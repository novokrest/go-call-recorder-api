package model

type RegisterPhoneResponse struct {
    Status string `json:"status"`
    Phone string `json:"phone"`
    Code string `json:"code"`
    Msg string `json:"msg"`
}

