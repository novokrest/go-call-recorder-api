package model

type GetFoldersResponseFolder struct {
    Id int64 `json:"id"`
    Name string `json:"name"`
    Created int64 `json:"created"`
    Updated int64 `json:"updated"`
    IsStart int32 `json:"is_start"`
    OrderId int64 `json:"order_id"`
}

