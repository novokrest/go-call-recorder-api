package model

type GetFilesRequest struct {
    ApiKey string `json:"api_key"`
    Page string `json:"page"`
    FolderId int64 `json:"folder_id"`
    Source string `json:"source"`
    Pass string `json:"pass"`
    Reminder bool `json:"reminder"`
    Q string `json:"q"`
    Id int64 `json:"id"`
    Op string `json:"op"`
}

