package model

type UpdateOrderResponse struct {
    Status string `json:"status"`
    Msg string `json:"msg"`
    Code string `json:"code"`
}

