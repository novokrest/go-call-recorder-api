package model

type RegisterPhoneRequest struct {
    Token string `json:"token"`
    Phone string `json:"phone"`
}

