package model

type GetFilesResponse struct {
    Status string `json:"status"`
    Credits int64 `json:"credits"`
    CreditsTrans int64 `json:"credits_trans"`
    Files []GetFilesResponseFile `json:"files"`
}

