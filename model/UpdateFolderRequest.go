package model

type UpdateFolderRequest struct {
    ApiKey string `json:"api_key"`
    Id int64 `json:"id"`
    Name string `json:"name"`
    Pass string `json:"pass"`
    IsPrivate bool `json:"is_private"`
}

