package model

type CreateFileData struct {
    Name string `json:"name"`
    Email string `json:"email"`
    Phone string `json:"phone"`
    LName string `json:"l_name"`
    FName string `json:"f_name"`
    Notes string `json:"notes"`
    Tags string `json:"tags"`
    Source string `json:"source"`
    RemindDays string `json:"remind_days"`
    RemindDate string `json:"remind_date"`
}

